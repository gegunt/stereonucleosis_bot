import os
import telebot
import schedule
import time
import threading
import random
import json
from openai import OpenAI
from dotenv import load_dotenv

# Загрузка переменных окружения из .env файла
load_dotenv()

# Получение значений переменных окружения
BOT_TOKEN = os.getenv('TELEGRAM_BOT_TOKEN')
OPENAI_API_KEY = os.getenv('OPENAI_API_KEY')
CHANNEL_ID = os.getenv('CHANNEL_ID')

bot = telebot.TeleBot(BOT_TOKEN)
client = OpenAI(api_key=OPENAI_API_KEY)

# Имя файла для хранения истории постов
HISTORY_FILE = 'post_history.json'

def load_post_history():
    if os.path.exists(HISTORY_FILE):
        with open(HISTORY_FILE, 'r', encoding='utf-8') as f:
            return json.load(f)
    return []

def save_post_history(history):
    with open(HISTORY_FILE, 'w', encoding='utf-8') as f:
        json.dump(history, f, ensure_ascii=False, indent=2)

def generate_post():
    history = load_post_history()
    
    # Ограничиваем контекст последними 10 постами
    context = "\n".join(history[-10:])
   
    prompt = f"""\Создай короткий пост (максимум 3 предложения) для AI-группы, которая называется \"Сереонуклеоз\" на свободную тему. Учитывай следующие правила:
    1. Шутки приветствуются
    2. Не повторяйся и не бойся говорить о новых вещах
    3. Излишняя вежливость неуместна
    4. Не нужно здороваться
    5. Общайся без пафоса
    6. Обращайся к слушателю на Ты
    7. Запрещены упоминания концертов и фестивалей
    8. Запрещено выдумывать новые релизы, треки, песни, синглы
    9. Можно задать вопрос к аудитории, но не злоупотребляй этим
    10. Создай уникальный пост, темы должны отличаться от предыдущих
    Вот последние посты, которые были написаны (если есть):\n\n{context}\n\nСоздай новый пост, учитывая эту историю и правила."""
    
    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[{"role": "system", "content": prompt}]
    )
    
    new_post = response.choices[0].message.content
    history.append(new_post)
    save_post_history(history)
    return new_post

def send_scheduled_post():
    try:
        post = generate_post()
        bot.send_message(CHANNEL_ID, post)
        print(f"Post sent: {post}")
    except Exception as e:
        print(f"Error sending post: {str(e)}")

@bot.message_handler(commands=['generate_post'])
def handle_generate_post(message):
    try:
        post = generate_post()
        bot.reply_to(message, f"Сгенерированный пост:\n\n{post}")
    except Exception as e:
        bot.reply_to(message, f"Произошла ошибка при генерации поста: {str(e)}")

@bot.message_handler(commands=['post_to_channel'])
def handle_post_to_channel(message):
    try:
        post = generate_post()
        bot.send_message(CHANNEL_ID, post)
        bot.reply_to(message, f"Пост успешно отправлен в канал:\n\n{post}")
    except Exception as e:
        bot.reply_to(message, f"Произошла ошибка при отправке поста: {str(e)}")

def schedule_random_posts():
    schedule.clear()  # Сбрасываем все запланированные задачи
    post_count = random.randint(0, 2)  # Планируем от 0 до 2 постов в день
    for _ in range(post_count):
        random_hour = random.randint(9, 22)
        random_minute = random.randint(0, 59)
        random_time = f"{random_hour:02d}:{random_minute:02d}"
        schedule.every().day.at(random_time).do(send_scheduled_post)
        print(f"Scheduled post at {random_time}")

def schedule_checker():
    while True:
        schedule.run_pending()
        time.sleep(1)

if __name__ == "__main__":
    # Планируем посты на текущий день
    schedule_random_posts()
    # Запланировать посты на следующий день
    schedule.every().day.at("00:00").do(schedule_random_posts)

    # Запуск потока для проверки расписания
    schedule_thread = threading.Thread(target=schedule_checker)
    schedule_thread.daemon = True
    schedule_thread.start()

    # Запуск бота
    print("Bot is running...")
    bot.polling(none_stop=True)